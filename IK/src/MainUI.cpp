#include "MainUI.h"

#define BUTTON_COLOR (Color){125, 125, 125, 170}
#define TEXT_COLOR (Color){215, 215, 215, 255}
#define BUTTON_HIGHLIGHTED_COLOR (Color){180, 180, 180, 255}

Rectangle BackRect()
{
    return (Rectangle){2, 2, 40, 40};
}

Rectangle PlayRect()
{
    return (Rectangle){44, 2, 80, 40};
}

Rectangle ForwardRect()
{
    return (Rectangle){126, 2, 40, 40};
}

Rectangle WriteFrameRect()
{
    return (Rectangle){168, 2, 200, 40};
}

Rectangle SaveRect()
{
    return (Rectangle){2, GetScreenHeight() - 42, 80, 40};
}

Rectangle LoadButtonRect()
{
    return (Rectangle){84, GetScreenHeight() - 42, 80, 40};
}

Rectangle LoadFilePathRect()
{
    return (Rectangle){166, GetScreenHeight() - 42, GetScreenWidth() - 168, 40};
}

/// @brief Draws a button and updates its state
/// @param rect The rectangle of the button 
/// @param mousePos The current mouse position
/// @param mousePressed Whether the mouse is pressed
/// @param buttonPressedPtr The button state
/// @param text The button text
void UpdateButton(Rectangle rect, Vector2 mousePos, bool mousePressed, bool *buttonPressedPtr, std::string text)
{
    bool buttonPressed = false;
    Color buttonColor = BUTTON_COLOR;

    // Check if the mouse is inside the button
    if(CheckCollisionPointRec(mousePos, rect))
    {
        buttonColor = BUTTON_HIGHLIGHTED_COLOR;

        if(mousePressed)
        {
            buttonPressed = true;
            buttonColor = BUTTON_COLOR;
        }
    }

    // Draw the button
    DrawRectangleRec(rect, buttonColor);
    DrawText(text.c_str(), rect.x + (rect.width / 2) - 7 * text.size(), rect.y + (rect.height / 2) - 10, 20, TEXT_COLOR);

    // Update state
    *buttonPressedPtr = buttonPressed;
}

/// @brief Draws and updates a text input field
/// @param rect The rectangle of the field
/// @param mousePos The current mouse position
/// @param mousePressed Whether the mouse is pressed
/// @param textboxSelectedPtr The textbox selection state
/// @param inputVector A vector storing what the user has typed
/// @param defaultText The default text of the textbox
void UpdateInputField(Rectangle rect, Vector2 mousePos, bool mousePressed, 
    bool *textboxSelectedPtr, std::vector<char>& inputVector, std::string defaultText)
{
    // If the mouse is pressed
    // Update whether the textbox is selected
    if(mousePressed)
    {
        if(CheckCollisionPointRec(mousePos, rect))
        {
            *textboxSelectedPtr = true;
        }
        else
        {
            *textboxSelectedPtr = false;
        }
    }

    Color buttonColor  = BUTTON_COLOR;

    // If the textbox is selected
    if(*textboxSelectedPtr)
    {
        buttonColor = BUTTON_HIGHLIGHTED_COLOR;

        // Get the character pressed by the keyboard
        int curKey = GetCharPressed();

        while (curKey > 0)
        {
            // Only accept keys within the standard range
            if((curKey >= 32) && (curKey <= 125))
            {
                // Remove the escape character from the vector (if it exists)
                if(inputVector.size() > 0)
                    inputVector.pop_back();

                // Add the currently pressed key
                inputVector.push_back((char)curKey);

                // Re-add the escape character
                inputVector.push_back('\0');
            }

            // Keep getting the pressed char until the keyboard queue is empty
            curKey = GetCharPressed();
        }

        //  If backspace is pressed and there's text in the input vector
        if(IsKeyPressed(KEY_BACKSPACE) && inputVector.size() > 1)
        {
            // Remove the escape character
            inputVector.pop_back();
            // Remove the last character
            inputVector.pop_back();
            // Re-add the escape character
            inputVector.push_back('\0');
        }
    }

    // Draw the textbox
    DrawRectangleRec(rect, buttonColor);
    if(inputVector.size() > 0)
    {
        // If there's text, draw the text of the input vector
        DrawText(&inputVector[0], rect.x + 2, rect.y + 2, 20, (Color){255,0,0,255});
    }
    else
    {
        // If there isn't draw the default text
        DrawText(defaultText.c_str(), rect.x + 2, rect.y + 2, 20, (Color){240,240,240,255});
    }
}

/// @brief Updates the UI for the BVH player
/// @param state The state of the UI
void UpdateUI(MainUIState *state)
{
    bool mousePressed = IsMouseButtonPressed(MOUSE_BUTTON_LEFT);
    Vector2 mousePos = GetMousePosition();

    // Back
    UpdateButton(BackRect(), mousePos, mousePressed, &state->decrementFramePressed, "<");

    // Forward
    UpdateButton(ForwardRect(), mousePos, mousePressed, &state->incrementFramePressed, ">");

    // Play
    std::string playText = state->play? "STOP" : "PLAY";
    UpdateButton(PlayRect(), mousePos, mousePressed, &state->playPressed, playText);
    if(state->playPressed)
    {
        state->play = !state->play;
    }

    // Write Frame
    if(!state->play)
        UpdateButton(WriteFrameRect(), mousePos, mousePressed, &state->writeFramePressed, "WRITE FRAME");
    else
        state->writeFramePressed = false;

    // Save
    UpdateButton(SaveRect(), mousePos, mousePressed, &state->savePressed, "SAVE");

    // Load
    UpdateButton(LoadButtonRect(), mousePos, mousePressed, &state->loadPressed, "LOAD");

    // File name input field
    UpdateInputField(LoadFilePathRect(), mousePos, mousePressed, &state->loadTextboxSelected, 
        state->inputFileName, "Enter filepath here...");
}

/// @brief Creates a default UI state
/// @return The state
MainUIState CreateUIState()
{
    MainUIState mainUIState;
    mainUIState.decrementFramePressed = false;
    mainUIState.incrementFramePressed = false;
    mainUIState.playPressed = false;
    mainUIState.play = true;
    mainUIState.savePressed = false;
    mainUIState.loadPressed = false;
    mainUIState.loadTextboxSelected = false;
    return mainUIState;
}