#pragma once
#include "Joint.h"
#include "raylib.h"
#include "BVHSkeleton.h"
#include "HandleData.h"

#define HANDLE_SIZE 0.6f
#define HANDLE_COLLISION_SIZE 0.7f
#define HANDLE_COLOR (Color){0,0,255,255}
#define HANDLE_COLOR_HIGHLIGHTED (Color){125,125,255,255}
#define HANDLE_COLOR_SELECTED (Color){255,255,0,255}
#define HANDLE_COLOR_TARGETED (Color){125,0,0,255}

#define IK_HANDLE_COLOR (Color){255,255,0,255}

/// @brief Updates a joints IK handles
/// @param skeleton The skeleton to update ik handles for
/// @param camera The camera viewing the skeleton
/// @param selectionData Current handle data
/// @param skipUpdate Whether to skip the update
void UpdateHandles(BVHSkeleton *skeleton, Camera *camera, HandleData *selectionData, bool skipUpdate);
