#pragma once
#include "MathHelpers.h"
#include "raylib.h"
#include "raymath.h"
#include "Joint.h"
#include "BVHSkeleton.h"
#include <map>
#include "Handle.h"

/// @brief Updates the world position coordinates of each joint in a skeleton
void UpdateSkeletonCoordinates(BVHSkeleton *skeleton);

/// @brief Computes an inverse kinematics iteration based on a collection of targets and their corresponding joints
/// @param joints The joints subject to positional constraints
/// @param targets The joint targets
/// @param numPairs The number of joint target pairs
/// @param skeleton The skeleton to compute IK on
void ComputeInverseKinematicsIteration(Joint **joints, Vector3 *targets, int numPairs, BVHSkeleton *skeleton);

/// @brief Computes an inverse kinematics iteration based on a collection of targets and their corresponding joints
/// @param jointHandlePairs A map of joints subject to positional constraints and their IK handles
/// @param skeleton The skeleton to compute IK on
void ComputeInverseKinematicsIteration(std::map<Joint *, Handle>& jointHandlePairs, BVHSkeleton *skeleton);