#pragma once
#include "raylib.h"
#include "raymath.h"

/// @brief Multiplies a raylib matrix and a raylib vector 4
/// @param m The matrix
/// @param v The vector
/// @return The resulting vector 4
Vector4 Matrix4VectorMultiplication(Matrix m, Vector4 v);

/// @brief Creates a rotation matrix around the X axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixX(float radians);

/// @brief Creates a rotation matrix around the Y axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixY(float radians);

/// @brief Creates a rotation matrix around the Z axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixZ(float radians);