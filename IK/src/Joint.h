#pragma once
#include "raylib.h"
#include "raymath.h"
#include "MathHelpers.h"
#include <vector>
#include <string>

const std::string XposSymbol = "Xposition";
const std::string YposSymbol = "Yposition";
const std::string ZposSymbol = "Zposition";

const std::string XrotSymbol = "Xrotation";
const std::string YrotSymbol = "Yrotation";
const std::string ZrotSymbol = "Zrotation";

class Joint
{
public:
    /// @brief Name of the joint
    std::string name;

    /// @brief The initial offset of the joint
    Vector3 initialsOffset;

    //Transformation matrices
    Matrix translationMatrix;
    Matrix rotationMatrix;

    // Tree structure
    std::vector<Joint*> childJoints;
    Joint *parentJoint;

    /// @brief Pointer to the channel buffer
    float* channelBuffer;

    // Offsets of positiional channels
    int xposChannelIndex;
    int yposChannelIndex;
    int zposChannelIndex;

    // Offsets of rotational channels
    int xrotChannelIndex;
    int yrotChannelIndex;
    int zrotChannelIndex;

    // The cached world position of the joint
    Vector3 worldPosition;

    Joint();

    /// @brief Sets the channel buffer for this joint and all children
    /// @param bufferPtr Pointer to the skeletons buffer
    void RecursivelySetChannelBuffer(float *bufferPtr);

    /// @brief Set the channel index of a channel
    /// @param channelName The name of the channel, read from a BVH file
    /// @param index The index of this channel
    void SetChannelIndex(std::string channelName, int index);

    // Position channels
    inline float *XposChannel()
    { return (xposChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[xposChannelIndex] : nullptr; }

    inline float *YposChannel()
    { return (yposChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[yposChannelIndex] : nullptr; }

    inline float *ZposChannel()
    { return (zposChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[zposChannelIndex] : nullptr; }

    // Rotation channels
    inline float *XrotChannel()
    { return (xrotChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[xrotChannelIndex] : nullptr; }

    inline float *YrotChannel()
    { return (yrotChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[yrotChannelIndex] : nullptr; }

    inline float *ZrotChannel()
    { return (zrotChannelIndex >= 0 && channelBuffer != nullptr)? &channelBuffer[zrotChannelIndex] : nullptr; }

    /// @brief Update a joints transformation matrices
    void UpdateMatrix();
    /// @brief Update the matrices of this joint and its children
    void RecursivelyUpdateMatrix();
};
