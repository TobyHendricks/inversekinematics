#include "BVHSkeleton.h"

using namespace std;

/// @brief Default constructor for a BVH skeleton. Reads the skeleton from a file
/// @param fileName 
BVHSkeleton::BVHSkeleton(string fileName)
{
    channelCounter = 0;
    currentFrame = -1;
    filePath = fileName;
    ReadFromFile(fileName);
}

/// @brief Reads a skeleton from a file
/// @param fileName The file path
void BVHSkeleton::ReadFromFile(string fileName)
{
    // Open the stream and check its validity
    ifstream fileStream;
    fileStream.open(fileName.c_str());

    if(fileStream.bad())
    {
        return;
    }

    // Read the heirarchy symbol
    string symbol;
    fileStream >> symbol;

    // If no heirarchy symbol found return
    if(symbol != hierarchySymbol)
    {
        return;
    }
    
    ReadHierarchy(fileStream);
    ReadMotion(fileStream);

    fileStream.close();
}

/// @brief Read the heirarchy component of a BVH file
/// @param fileStream The stream reading the file
void BVHSkeleton::ReadHierarchy(ifstream& fileStream)
{
    string symbol;
    do
    {
        fileStream >> symbol;

        // Look for a ROOT symbol
        if(symbol == rootSymbol)
        {
            // Read the root joint
            Joint *newJoint = ReadJoint(fileStream);
            roots.push_back(newJoint);
        }


    }while(symbol != motionSymbol && !fileStream.eof());

    // Allocate the channel buffer
    channelBuffer = (float*)malloc(sizeof(float) * channelCounter);

    // Initialise it to 0s
    for(int i = 0; i < channelCounter; channelBuffer[i++] = 0.0f);

    // Go through all the roots
    for(unsigned int i = 0; i < roots.size(); ++i)
    {
        // Set the channel buffer for all joints
        roots[i]->RecursivelySetChannelBuffer(channelBuffer);

        // Add IK joints
        // The root is not counted as an IK joint so begin at its children
        for(unsigned int j = 0; j < roots[i]->childJoints.size(); ++j)
        {
            RecursivelyDiscoverIkChannels(roots[i]->childJoints[j]);
        }
        
    }
}

/// @brief Read a joint from a BVH file
/// @param fileStream The stream reading the file
/// @return A pointer to the joint
Joint* BVHSkeleton::ReadJoint(ifstream& fileStream)
{
    Joint *newJoint = new Joint();
    
    // Read the name
    fileStream >> newJoint->name;
    std::cout << "Reading Joint " << newJoint->name << std::endl;

    string symbol;
    do
    {
        fileStream >> symbol;

        // Read OFFSET data
        if(symbol == offsetSymbol)
        {
            // Offset symbol is followed by a vector3
            fileStream >> newJoint->initialsOffset.x >> newJoint->initialsOffset.y >> newJoint->initialsOffset.z;
        }
        else if(symbol == jointSymbol)
        {
            // Read a child joint of this joint
            Joint *childJoint = ReadJoint(fileStream);

            newJoint->childJoints.push_back(childJoint);
            childJoint->parentJoint = newJoint;
        }
        else if(symbol == channelsSymbol)
        {
            // Read the channels of a joint
            int numChannels;
            fileStream >> numChannels;

            for(int i = 0; i < numChannels; ++i)
            {
                string channelName;
                fileStream >> channelName;

                newJoint->SetChannelIndex(channelName, channelCounter++);
            }
        }
        else if(symbol == endSymbol)
        {
            // Read the end site of a joint
            ReadEndSite(fileStream);
        }

    }while(symbol != "}" && !fileStream.eof());

    return newJoint;
}

/// @brief Read the end site
/// @param fileStream The stream reading the file
void BVHSkeleton::ReadEndSite(ifstream& fileStream)
{
    string symbol;
    do
    {
        fileStream >> symbol;

    }while(symbol != "}" && !fileStream.eof());
}

/// @brief Reat the motion component of a BVH file
/// @param fileStream The stream reading the file
void BVHSkeleton::ReadMotion(ifstream& fileStream)
{
    string symbol;

    // Read the number of frames
    fileStream >> symbol;

    if(symbol != "Frames:")
    {
        return;
    }

    fileStream >> numFrames;

    // Read frame time
    fileStream >> symbol;
    fileStream >> symbol;

    if(symbol != "Time:")
    {
        return;
    }

    fileStream >> frameTime;

    // Allocate memory for each frame
    unsigned int bufferSize = channelCounter * numFrames;
    motionBuffer = (float*)malloc(sizeof(float) * bufferSize);

    // Read the motion data into the motion buffer
    unsigned int i;
    for(i = 0; i < bufferSize && !fileStream.eof(); ++i)
    {
        fileStream >> motionBuffer[i];
    }

    // Error if we make it to the end of the file before we reach the number of channels
    if(i != bufferSize)
    {
        std::cout << "Read error detected. Number of read values not equal to the number of channels * the number of frames" << std::endl;
    }
}

/// @brief Sets the current frame of the skeletons animation. Transfers the frame data from the motion buffer to the channel buffer
/// @param frameNumber 
void BVHSkeleton::SetFrame(int frameIndex)
{
    // Set the frame number
    currentFrame = frameIndex;

    // Calculate where the frame is in the motion buffer
    int frameOffset = frameIndex * channelCounter;
    // Get that address
    float *frameArray = &motionBuffer[frameOffset];

    // Copy it from the motion buffer to the channel buffer
    memcpy(channelBuffer, frameArray, sizeof(float) * channelCounter);

    // Update the transform matrices of each joint
    UpdateJointMatrices();
}

/// @brief Sets the frame to the next frame
void BVHSkeleton::IncrementFrame()
{
    int f = (currentFrame + 1) % numFrames;
    SetFrame(f);
}

/// @brief Sets the frame to the previous frame
void BVHSkeleton::DecrementFrame()
{
    int f = (currentFrame + (numFrames - 1)) % numFrames;
    SetFrame(f);
}

/// @brief Resets the frame to the current frame
void BVHSkeleton::ResetFrame()
{
    SetFrame(currentFrame);
}

/// @brief Writes the frame to the motion buffer, allowing changes to the frame to remain persistent
void BVHSkeleton::WriteFrame()
{
    // Calculate the offset in the motion buffer
    int frameOffset = currentFrame * channelCounter;
    // Get that address
    float *frameArray = &motionBuffer[frameOffset];

    // Copy the channel buffer into the corresponding frame of the motion buffer
    memcpy(frameArray, channelBuffer, sizeof(float) * channelCounter);
}

/// @brief Discover channels used for IK. Rotaion channels are used for IK
/// @param joint The joint to search in
void BVHSkeleton::RecursivelyDiscoverIkChannels(Joint* joint)
{
    // Add this joints IK channels
    float* channels[3];
    channels[0] = joint->XrotChannel();
    channels[1] = joint->YrotChannel();
    channels[2] = joint->ZrotChannel();

    for(int i = 0; i < 3; ++i)
    {
        if(channels[i] != nullptr)
        {
            ikChannels.push_back(channels[i]);
        }
    }

    // Recurse
    for(unsigned int i = 0; i < joint->childJoints.size(); ++i)
    {
        RecursivelyDiscoverIkChannels(joint->childJoints[i]);
    }
}

/// @brief Updates the transformation matrices of all joints in the skeleton
void BVHSkeleton::UpdateJointMatrices()
{
    for(unsigned int i = 0; i < roots.size(); roots[i++]->RecursivelyUpdateMatrix());
}

/// @brief Save the skeleton to a file
void BVHSkeleton::SaveToFile()
{
    // Open the output stream
    std::ofstream outputStream;
    outputStream.open(filePath);

    // Write the heirarchy symbol
    outputStream << hierarchySymbol << std::endl;

    // Write the joints
    for(unsigned int i = 0; i < roots.size(); ++i)
    {
        RecursivelyWriteJoints(outputStream, roots[i], rootSymbol, 0);
    }

    // Write the motion symbols and the motion metadata
    outputStream << motionSymbol << endl;
    outputStream << "Frames: " << numFrames << std::endl;
    outputStream << "Frame Time: " << frameTime << std::endl;

    // Write the motion data
    for(int i = 0; i < numFrames; ++i)
    {
        std::stringstream numericStringStream;
        numericStringStream << std::fixed;
        numericStringStream.precision(6);

        for(int j = 0; j < channelCounter; ++j)
        {
            numericStringStream << motionBuffer[i * channelCounter + j] << " ";
        }

        outputStream << numericStringStream.str() << std::endl;
    }

    outputStream.close();
}

/// @brief Writes tabs to an output stream
/// @param outputStream 
/// @param num Number of tabs
inline void AddTabs(std::ofstream& outputStream, int num)
{
    for(int n = 0; n < num; ++n)
    {
        outputStream << tab;
    }
}

/// @brief Writes a joint and its children to a file
/// @param outputStream The file stream to write to
/// @param joint The joint to write
/// @param headerSymbol The header symbol of the joint (either ROOT or JOINT)
/// @param tabs The number of tabs
void BVHSkeleton::RecursivelyWriteJoints(std::ofstream& outputStream, Joint *joint, std::string headerSymbol, int tabs)
{
    // Joint header
    AddTabs(outputStream, tabs);
    outputStream << headerSymbol << " " << joint->name << std::endl;

    AddTabs(outputStream, tabs);
    outputStream << "{" << std::endl;

    // Offset
    // Get initial offset in string form
    std::stringstream vectorStringStream;
    vectorStringStream << std::fixed;
    vectorStringStream.precision(6);

    vectorStringStream << joint->initialsOffset.x << " " << joint->initialsOffset.y << " " << joint->initialsOffset.z;

    // Output offset
    AddTabs(outputStream, tabs + 1);
    outputStream << offsetSymbol << " " << vectorStringStream.str() << std::endl;

    // Channels
    // Get channels
    std::vector<string> channels;

    // Position channels
    if(joint->XposChannel() != nullptr)
        channels.push_back(XposSymbol);

    if(joint->YposChannel() != nullptr)
        channels.push_back(YposSymbol);

    if(joint->ZposChannel() != nullptr)
        channels.push_back(ZposSymbol);

    // Rotation channels
    if(joint->ZrotChannel() != nullptr)
        channels.push_back(ZrotSymbol);

    if(joint->YrotChannel() != nullptr)
        channels.push_back(YrotSymbol);

    if(joint->XrotChannel() != nullptr)
        channels.push_back(XrotSymbol);

    // Output chanels
    AddTabs(outputStream, tabs + 1);
    outputStream << channelsSymbol << " " << channels.size();
    
    for(unsigned int i = 0; i < channels.size(); ++i)
    {
        outputStream << " " << channels[i];
    }
    outputStream << std::endl;

    if(joint->childJoints.size() > 0)
    {
        // Recurse
        for(unsigned int i = 0; i < joint->childJoints.size(); ++i)
        {
            RecursivelyWriteJoints(outputStream, joint->childJoints[i], jointSymbol, tabs + 1);
        }
    }
    else
    {
        // TODO handle end sites properly
        AddTabs(outputStream, tabs + 1);
        outputStream << "End Site" << std::endl;

        AddTabs(outputStream, tabs + 1);
        outputStream << "{" << std::endl;

        AddTabs(outputStream, tabs + 2);
        outputStream << "OFFSET 0.000000 0.000000 0.000000" << std::endl;

        AddTabs(outputStream, tabs + 1);
        outputStream << "}" << std::endl;
    }


    // End joint
    AddTabs(outputStream, tabs);
    outputStream << "}" << std::endl;
}

/// @brief Deletes a joint and frees its allocated memory
/// @param joint The joint to delete
void BVHSkeleton::DeleteJoint(Joint *joint)
{
    for(unsigned int i = 0; i < joint->childJoints.size(); ++i)
    {
        DeleteJoint(joint->childJoints[i]);
    }

    joint->childJoints.clear();
    delete joint;
}

/// @brief Deletes all joints and frees all buffers
BVHSkeleton::~BVHSkeleton()
{
    for(unsigned int i = 0; i < roots.size(); ++i)
    {
        DeleteJoint(roots[i]);
    }

    roots.clear();
    ikChannels.clear();
    free(channelBuffer);
    free(motionBuffer);
}