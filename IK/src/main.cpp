#include "raylib.h"
#include "BVHSkeleton.h"
#include "SkeletonRenderer.h"
#include "JointManipulator.h"
#include "HandleData.h"
#include "MainUI.h"
#include "Kinematics.h"
#include "CameraController.h"
#include <unistd.h>
#include <iostream>

/// @brief Loads a skeleton if the given file path exists
/// @param filePath The filepath to load
/// @return A pointer to the created skeleton
BVHSkeleton * TryLoadSkeleton(char* filePath);

/// @brief Updates a skeletons joints, handles and ik
/// @param skeleton The skeleton to update
/// @param mainUIState The state of the UI
/// @param jointSelection The currently selected joint/ik handles
/// @param camera The camera viewing the skeleton
void UpdateSkeleton(BVHSkeleton *skeleton, MainUIState *mainUIState, HandleData *jointSelection, Camera3D *camera);

/// @brief Setup a given camera
void SetupCamera(Camera3D *camera);

int main(int argv, char **argc)
{
    // Create the window
    const int screenWidth = 900;
    const int screenHeight = 600;

    InitWindow(screenWidth, screenHeight, "Animation Coursework 1");
    SetTargetFPS(100);

    // Define background colour
    Color white = {240, 240, 240, 255};

    // Setup the camera
    Camera3D camera;
    SetupCamera(&camera);

    // Setup initialise the skeleton and the selection data
    BVHSkeleton *skelly = nullptr;
    HandleData jointSelection = {nullptr, nullptr, false, false};

    // Create the UI state
    MainUIState mainUIState = CreateUIState();
    
    // Main loop
    while (!WindowShouldClose())
    {
        // Reset selection if the user has pressed forward, backward or play
        if(mainUIState.playPressed || mainUIState.decrementFramePressed || mainUIState.incrementFramePressed)
        {
            jointSelection.currentHandles.clear();
            jointSelection.selectedHandle = nullptr;
            jointSelection.selectedJoint = nullptr;
        }

        // Update the camera
        UpdateCameraTransform(&camera);
        UpdateCamera(&camera);

        // Initialise drawing in Raylib
        BeginDrawing();
        BeginMode3D(camera);

        // Draw the initial scene
        ClearBackground(white);
        // Draw a raylib grid
        DrawGrid(20, 5);

        // If we have a skeleton
        if(skelly != nullptr)
        {
            UpdateSkeleton(skelly, &mainUIState, &jointSelection, &camera);
        } // if (skelly != nullptr)

        // If the user has pressed load
        if(mainUIState.loadPressed)
        {
            // Load the skeleton
            skelly = TryLoadSkeleton(&mainUIState.inputFileName[0]);
            mainUIState.inputFileName.clear();
        }

        // Exit 3D drawing mode
        EndMode3D();

        // Update the UI
        UpdateUI(&mainUIState);

        // End drawing
        EndDrawing();
    } // while (!WindowShouldClose())

    CloseWindow();

    return 0;
}

/// @brief Loads a skeleton if the given file path exists
/// @param filePath The filepath to load
/// @return A pointer to the created skeleton
BVHSkeleton * TryLoadSkeleton(char* filePath)
{
    if(access( filePath, F_OK ) != -1)
    {
        BVHSkeleton * skeleton = new BVHSkeleton(filePath);
        return skeleton;
    }

    return nullptr;
}

/// @brief Setup a given camera
void SetupCamera(Camera3D *camera)
{
    camera->position = (Vector3){60.0f, 60.0f, 60.0f}; 
    camera->target = (Vector3){0.0f, 0.0f, 0.0f};      
    camera->up = (Vector3){0.0f, 1.0f, 0.0f};         
    camera->fovy = 45.0f;                              
    camera->projection = CAMERA_PERSPECTIVE;
    SetCameraMode(*camera, CAMERA_PERSPECTIVE);   
}

/// @brief Updates a skeletons joints, handles and ik
/// @param skeleton The skeleton to update
/// @param mainUIState The state of the UI
/// @param jointSelection The currently selected joint/ik handles
/// @param camera The camera viewing the skeleton
void UpdateSkeleton(BVHSkeleton *skeleton, MainUIState *mainUIState, HandleData*jointSelection, Camera3D *camera)
{
    // UI input reading
    // If the skeleton animation is playing
    if(mainUIState->play)
    {
        // Increment the frame every frame
        skeleton->IncrementFrame();
    }
    else
    {
        // Otherwise increment or decrement the frame based on user input
        if(mainUIState->decrementFramePressed)
        {
            skeleton->DecrementFrame();
        }
        else if(mainUIState->incrementFramePressed)
        {
            skeleton->IncrementFrame();
        }

        // If the user presses write frame
        if(mainUIState->writeFramePressed)
        {
            // Write the frame
            skeleton->WriteFrame();

            //Clear ik
            jointSelection->currentHandles.clear();
            jointSelection->selectedHandle = nullptr;
            jointSelection->selectedJoint = nullptr;
        }
    }

    // If the user presses save, save to file
    if(mainUIState->savePressed)
    {
        skeleton->SaveToFile();
    }

    // The frame has been set, use FK to update the world coordinates of each joint
    UpdateSkeletonCoordinates(skeleton);
    // Draw the skeleton
    RenderSkeleton(skeleton, !mainUIState->play);
    
    // Update the skeletons joint and ik handles
    UpdateHandles(skeleton, camera, jointSelection, mainUIState->play);
    // Compute IK using the updated ik handles
    ComputeInverseKinematicsIteration(jointSelection->currentHandles, skeleton);

    // If the user presses load delete this skeleton
    if(mainUIState->loadPressed)
    {
        delete skeleton;
        jointSelection->currentHandles.clear();
        jointSelection->selectedHandle = nullptr;
        jointSelection->selectedJoint = nullptr;
    }
}