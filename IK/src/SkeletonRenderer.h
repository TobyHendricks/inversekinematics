#pragma once
#include "raylib.h"
#include "BVHSkeleton.h"
#include "Kinematics.h"

/// @brief Draw a skeleton using Raylib primitives
/// @param skeleton The skeleton
/// @param lines Whether to draw the skeleton with lines or cylinders
void RenderSkeleton(BVHSkeleton *skeleton, bool lines);


