#pragma once
#include "raylib.h"
#include <string>
#include <vector>

struct MainUIState
{
    // Button state
    bool playPressed;
    bool incrementFramePressed;
    bool decrementFramePressed;
    bool writeFramePressed;
    bool savePressed;
    bool loadPressed;
    bool loadTextboxSelected;

    // Current state
    bool play;
    std::vector<char> inputFileName;
};

/// @brief Updates the UI for the BVH player
/// @param state The state of the UI
void UpdateUI(MainUIState *state);

/// @brief Creates a default UI state
/// @return The state
MainUIState CreateUIState();
