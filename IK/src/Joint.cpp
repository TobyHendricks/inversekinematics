#include "Joint.h"
#include <iostream>

using namespace std;

Joint::Joint()
{
    channelBuffer = nullptr;
    xposChannelIndex = -1;
    yposChannelIndex = -1;
    zposChannelIndex = -1;

    xrotChannelIndex = -1;
    yrotChannelIndex = -1;
    zrotChannelIndex = -1;

    initialsOffset = Vector3Zero();
    translationMatrix = MatrixIdentity();
    rotationMatrix = MatrixIdentity();

    worldPosition = Vector3Zero();
}

/// @brief Set the channel index of a channel
/// @param channelName The name of the channel, read from a BVH file
/// @param index The index of this channel
void Joint::SetChannelIndex(string channelName, int index)
{
    if(channelName == XposSymbol)
    {
        std::cout << "Joint " << name << ": adding Xpos to Channel " << index << std::endl;
        xposChannelIndex = index;
    }
    else if(channelName == YposSymbol)
    {
        std::cout << "Joint " << name << ": adding Ypos to Channel " << index << std::endl;
        yposChannelIndex = index;
    }
    else if(channelName == ZposSymbol)
    {
        std::cout << "Joint " << name << ": adding Zpos to Channel " << index << std::endl;
        zposChannelIndex = index;
    }
    else if(channelName == XrotSymbol)
    {
        std::cout << "Joint " << name << ": adding Xrot to Channel " << index << std::endl;
        xrotChannelIndex = index;
    }
    else if(channelName == YrotSymbol)
    {
        std::cout << "Joint " << name << ": adding Yrot to Channel " << index << std::endl;
        yrotChannelIndex = index;
    }
    else if(channelName == ZrotSymbol)
    {
        std::cout << "Joint " << name << ": adding Zrot to Channel " << index << std::endl;
        zrotChannelIndex = index;
    }
}

/// @brief Update a joints transformation matrices
void Joint::UpdateMatrix()
{
    // Get rotation
    Vector3 eulerRotation;
    float* xRotBuffer = XrotChannel();
    float* yRotBuffer = YrotChannel();
    float* zRotBuffer = ZrotChannel();

    eulerRotation.x = (xRotBuffer == nullptr)? 0 : *xRotBuffer;
    eulerRotation.y = (yRotBuffer == nullptr)? 0 : *yRotBuffer;
    eulerRotation.z = (zRotBuffer == nullptr)? 0 : *zRotBuffer;

    // Get position
    Vector3 position;
    float *xPosBuffer = XposChannel();
    float *yPosBuffer = YposChannel();
    float *zPosBuffer = ZposChannel();

    position.x = (xPosBuffer == nullptr)? 0 : *xPosBuffer;
    position.y = (yPosBuffer == nullptr)? 0 : *yPosBuffer;
    position.z = (zPosBuffer == nullptr)? 0 : *zPosBuffer;

    // Construct matrices
    // Translation
    Matrix t = MatrixIdentity();
    t.m3 = position.x;
    t.m7 = position.y;
    t.m11 = position.z;

    translationMatrix = t;

    // Rotation
    Matrix xRot = RotationMatrixX(eulerRotation.x * DEG2RAD);
    Matrix yRot = RotationMatrixY(eulerRotation.y * DEG2RAD);
    Matrix zRot = RotationMatrixZ(eulerRotation.z * DEG2RAD);

    rotationMatrix = MatrixMultiply(MatrixMultiply(zRot, yRot), xRot);
}

/// @brief Update the matrices of this joint and its children
void Joint::RecursivelyUpdateMatrix()
{
    UpdateMatrix();

    for(int i = 0; i < childJoints.size(); ++i)
    {
        childJoints[i]->RecursivelyUpdateMatrix();
    }
}

/// @brief Sets the channel buffer for this joint and all children
/// @param bufferPtr Pointer to the skeletons buffer
void Joint::RecursivelySetChannelBuffer(float *bufferPtr)
{
    channelBuffer = bufferPtr;
    for(int i = 0; i < childJoints.size(); ++i)
    {
        childJoints[i]->RecursivelySetChannelBuffer(bufferPtr);
    }
}