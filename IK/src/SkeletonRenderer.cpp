#include "SkeletonRenderer.h"
#include <eigen3/Eigen/Dense>

Color green = {0, 255, 0, 255};

/// @brief Render a joint and its children
/// @param joint The joint to render
/// @param previousWorldPos The position of the previous joint
/// @param root Whether this is the root joint
/// @param lines Whether to draw the skeleton with lines or cylinders
void RenderJointRecursive(Joint *joint, Vector3 previousWorldPos, bool root, bool lines);

/// @brief Draw a skeleton using Raylib primitives
/// @param skeleton The skeleton
/// @param lines Whether to draw the skeleton with lines or cylinders
void RenderSkeleton(BVHSkeleton *skeleton, bool lines)
{
    for(int i = 0; i < skeleton->roots.size(); ++i)
    {
        RenderJointRecursive(skeleton->roots[i], Vector3Zero(), true, lines);
    }
}

/// @brief Render a joint and its children
/// @param joint The joint to render
/// @param previousWorldPos The position of the previous joint
/// @param root Whether this is the root joint
/// @param lines Whether to draw the skeleton with lines or cylinders
void RenderJointRecursive(Joint *joint, Vector3 previousWorldPos, bool root, bool lines)
{
    Vector3 cartesian = joint->worldPosition;

    if(!root)
    {
        if(!lines)
            DrawCylinderEx(previousWorldPos, cartesian, 0.6f, 0.2f, 3, green);
        else
            DrawLine3D(previousWorldPos, cartesian, green);
    }
        
    for(int i = 0; i < joint->childJoints.size(); ++i)
    {   
        RenderJointRecursive(joint->childJoints[i], cartesian, false, lines);
    }
}