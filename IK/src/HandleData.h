#pragma once
#include "Handle.h"
#include "Joint.h"
#include <map>

// Handle Data
struct HandleData
{
    Joint *selectedJoint;
    Handle *selectedHandle;
    bool onPressed;
    bool pressed;
    Vector3 startingPosition;
    std::map<Joint*, Handle> currentHandles;
};