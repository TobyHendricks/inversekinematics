#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Joint.h"

const std::string hierarchySymbol = "HIERARCHY";
const std::string motionSymbol = "MOTION";
const std::string offsetSymbol = "OFFSET";
const std::string jointSymbol = "JOINT";
const std::string channelsSymbol = "CHANNELS";
const std::string endSymbol = "End";
const std::string rootSymbol = "ROOT";
const std::string tab = "        ";

/// @brief A BVH skeleton. Joints and animation data.
class BVHSkeleton
{
private:
    /// @brief File path of the skeleton
    std::string filePath;

    /// @brief Reads a skeleton from a file
    /// @param fileName The file path
    void ReadFromFile(std::string fileName);

    /// @brief Read the heirarchy component of a BVH file
    /// @param fileStream The stream reading the file
    void ReadHierarchy(std::ifstream& fileStream);

    /// @brief Reat the motion component of a BVH file
    /// @param fileStream The stream reading the file
    void ReadMotion(std::ifstream& fileStream);

    /// @brief Read a joint from a BVH file
    /// @param fileStream The stream reading the file
    /// @return A pointer to the joint
    Joint* ReadJoint(std::ifstream& fileStream);

    /// @brief Read the end site
    /// @param fileStream The stream reading the file
    void ReadEndSite(std::ifstream& fileStream);

    /// @brief Searches through a joint and its children for IK channels
    /// @param joint The joint to search
    void RecursivelyDiscoverIkChannels(Joint* joint);

    /// @brief Recursively writes a joint and its children to a BVH file
    /// @param outputStream The output file stream
    /// @param joint The joint to write
    /// @param headerSymbol The header symbol of the joint (typically ROOT or JOINT)
    /// @param tabs The number of tabs to insert
    void RecursivelyWriteJoints(std::ofstream& outputStream, Joint *joint, std::string headerSymbol, int tabs);

    /// @brief Delete a joint and free its allocated memory
    /// @param joint The joint to delete
    void DeleteJoint(Joint * joint);
public:
    /// @brief A buffer containing joint transformations for the skeleton
    float *channelBuffer;
    /// @brief A buffer containing motion data for the skeleton
    float *motionBuffer;

    /// @brief Pointers to the channels in the channelBuffer utilised for IK
    std::vector<float*> ikChannels;

    /// @brief Counts and stores the number of channels in the skeleton
    int channelCounter;

    /// @brief The number of frames in the skeletons animation
    int numFrames;
    /// @brief The frame time of the animation
    float frameTime;
    /// @brief The current frame of the animation
    int currentFrame;

    /// @brief The joint tree of the skeletons
    std::vector<Joint*> roots;

    /// @brief Sets the current frame of the skeletons animation. Transfers the frame data from the motion buffer to the channel buffer
    /// @param frameNumber 
    void SetFrame(int frameNumber);
    /// @brief Sets the frame to the next frame
    void IncrementFrame();
    /// @brief Sets the frame to the previous frame
    void DecrementFrame();
    /// @brief Resets the frame to the current frame
    void ResetFrame();
    /// @brief Writes the frame to the motion buffer, allowing changes to the frame to remain persistent
    void WriteFrame();

    /// @brief Update the transformation matrices of every joint in the skeleton
    void UpdateJointMatrices();

    /// @brief Save the skeleton to a file
    void SaveToFile();

    BVHSkeleton(std::string fileName);
    ~BVHSkeleton();
};
