#pragma once
#include "raylib.h"
#include "raymath.h"

/// @brief Transforms a camera utilising Pan, Rotate and Zoom
/// @param camera The camera to transform
void UpdateCameraTransform(Camera3D *camera);