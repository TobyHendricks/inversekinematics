#include "CameraController.h"

const float cameraRotationSensitivity = 0.4f;
const float cameraPanSpeed = 0.5f;
const float cameraZoomSpeed = 0.8f;
const float cameraTargetDistance = 80;

/// @brief Pans a camera
/// @param camera The camera to pan
/// @param cameraForward The forward vector of the camera
/// @param cameraRight The right vector of the camera
/// @param mouseMovement The mouse movement
void PanCamera(Camera3D *camera, Vector3 cameraForward, Vector3 cameraRight, Vector2 mouseMovement);

/// @brief Rotate a camera
/// @param camera The camera to rotate
/// @param cameraForward The forward vector of the camera
/// @param cameraRight The right vector of the camera
/// @param mouseMovement The mouse movement
void RotateCamera(Camera3D *camera, Vector3 cameraForward, Vector3 cameraRight, Vector2 mouseMovement);

/// @brief Zooms a camera
/// @param camera The camera to zoom
/// @param cameraForward The forward vector of the camera
/// @param mouseWheel The mouse wheel input
void ZoomCamera(Camera3D *camera, Vector3 cameraForward, float mouseWheel);

/// @brief Transforms a camera utilising Pan, Rotate and Zoom
/// @param camera The camera to transform
void UpdateCameraTransform(Camera3D *camera)
{
    // Calculate forward and right vectors
    Vector3 cameraForward = Vector3Subtract(camera->target, camera->position);
    Vector3 cameraRight = Vector3CrossProduct(Vector3Normalize(camera->up), Vector3Normalize(cameraForward));

    // Get mouse input
    Vector2 mouseMovement = GetMouseDelta();
    float mouseWheel = GetMouseWheelMove();

    // Apply transformation
    if(IsMouseButtonDown(MOUSE_BUTTON_RIGHT))
    {
        if(IsKeyDown(KEY_LEFT_SHIFT))
        {
            RotateCamera(camera, cameraForward, cameraRight, mouseMovement);
        }
        else
        {
            PanCamera(camera, cameraForward, cameraRight, mouseMovement);
        }
    }

    ZoomCamera(camera, cameraForward, mouseWheel);
    
}

/// @brief Pans a camera
/// @param camera The camera to pan
/// @param cameraForward The forward vector of the camera
/// @param cameraRight The right vector of the camera
/// @param mouseMovement The mouse movement
void PanCamera(Camera3D *camera, Vector3 cameraForward, Vector3 cameraRight, Vector2 mouseMovement)
{
    // Calculate camera velocity
    cameraForward = Vector3Normalize(cameraForward);
    Vector3 horizontalVelocity = Vector3Scale(cameraRight, cameraPanSpeed * mouseMovement.x);
    Vector3 verticalVelocity = Vector3Scale(Vector3Normalize(camera->up), cameraPanSpeed * mouseMovement.y);
    Vector3 cameraVelocity = Vector3Add(horizontalVelocity, verticalVelocity);

    // Set the position and target
    camera->position = Vector3Add(camera->position, cameraVelocity);
    camera->target = Vector3Add(camera->target, cameraVelocity);
}

/// @brief Rotate a camera
/// @param camera The camera to rotate
/// @param cameraForward The forward vector of the camera
/// @param cameraRight The right vector of the camera
/// @param mouseMovement The mouse movement
void RotateCamera(Camera3D *camera, Vector3 cameraForward, Vector3 cameraRight, Vector2 mouseMovement)
{
    // Set the target to a set distance from the camera
    cameraForward = Vector3Normalize(cameraForward);
    camera->target = Vector3Add(camera->position, Vector3Scale(cameraForward, cameraTargetDistance));

    // Calculate target
    Vector3 horizontalVelocity = Vector3Scale(cameraRight, cameraRotationSensitivity * mouseMovement.x);
    Vector3 verticalVelocity = Vector3Scale(Vector3Normalize(camera->up), cameraRotationSensitivity * mouseMovement.y);
    Vector3 cameraVelocity = Vector3Add(horizontalVelocity, verticalVelocity);

    camera->position = Vector3Add(camera->position, cameraVelocity);

    // Set the position
    cameraForward = Vector3Subtract(camera->target, camera->position);
    cameraForward = Vector3Normalize(cameraForward);
    camera->position = Vector3Subtract(camera->target, Vector3Scale(cameraForward, cameraTargetDistance));
}

/// @brief Zooms a camera
/// @param camera The camera to zoom
/// @param cameraForward The forward vector of the camera
/// @param mouseWheel The mouse wheel input
void ZoomCamera(Camera3D *camera, Vector3 cameraForward, float mouseWheel)
{
    // Calculate the velocity
    cameraForward = Vector3Normalize(cameraForward);
    Vector3 forwardVelocity = Vector3Scale(cameraForward, cameraZoomSpeed * mouseWheel);

    // Set the position and target
    camera->position = Vector3Add(camera->position, forwardVelocity);
    camera->target = Vector3Add(camera->target, forwardVelocity);
}