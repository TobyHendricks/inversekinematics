#include "MathHelpers.h"

/// @brief Multiplies a raylib matrix and a raylib vector 4
/// @param m The matrix
/// @param v The vector
/// @return The resulting vector 4
Vector4 Matrix4VectorMultiplication(Matrix m, Vector4 v)
{
    Vector4 result;
    result.x = m.m0 * v.x + m.m1 * v.y + m.m2 * v.z + m.m3 * v.w;
    result.y = m.m4 * v.x + m.m5 * v.y + m.m6 * v.z + m.m7 * v.w;
    result.z = m.m8 * v.x + m.m9 * v.y + m.m10 * v.z + m.m11 * v.w;
    result.w = m.m12 * v.x + m.m13 * v.y + m.m14 * v.z + m.m15 * v.w;

    return result;    
}

/// @brief Creates a rotation matrix around the X axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixX(float radians)
{
    Matrix rot = MatrixIdentity();

    float cosAngle = cosf(radians);
    float sinAngle = sinf(radians);

    rot.m5 = cosAngle;
    rot.m6 = -sinAngle;
    rot.m9 = sinAngle;
    rot.m10 = cosAngle;

    return rot;
}

/// @brief Creates a rotation matrix around the Y axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixY(float radians)
{
    Matrix rot = MatrixIdentity();

    float cosAngle = cosf(radians);
    float sinAngle = sinf(radians);

    rot.m0 = cosAngle;
    rot.m2 = sinAngle;
    rot.m8 = -sinAngle;
    rot.m10 = cosAngle;

    return rot;
}

/// @brief Creates a rotation matrix around the Z axis
/// @param radians The amount of radians to rotate around
/// @return The rotation matrix
Matrix RotationMatrixZ(float radians)
{
    Matrix rot = MatrixIdentity();

    float cosAngle = cosf(radians);
    float sinAngle = sinf(radians);

    rot.m0 = cosAngle;
    rot.m1 = -sinAngle;
    rot.m4 = sinAngle;
    rot.m5 = cosAngle;

    return rot;
}