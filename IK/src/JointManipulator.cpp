#include "JointManipulator.h"
#include "Kinematics.h"

/// @brief Update the handles for a joint and its children
/// @param joint The joint to update
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current selection data
/// @param jointHighlighted Whether a joint has been highlightes
void UpdateJointHandleRecursive(Joint* joint, Ray mouseRay, HandleData *selectionData, bool *jointHighlighted);

/// @brief Update all IK handles
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current selection data
/// @param jointHighlighted Whether a joint has been highlightes
void UpdateIKHandles(Ray mouseRay, HandleData *selectionData, bool *jointHighlighted);

/// @brief Draws a handle
/// @param position
/// @param handleColor 
void DrawHandle(Vector3 position, Color handleColor);

/// @brief Computes the drag position of a handle based on mouse position
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current selection data
/// @return The target position of the handle
Vector3 ComputeTarget(Ray mouseRay, HandleData *selectionData);

/// @brief Updates a joints IK handles
/// @param skeleton The skeleton to update ik handles for
/// @param camera The camera viewing the skeleton
/// @param selectionData Current handle data
/// @param skipUpdate Whether to skip the update
void UpdateHandles(BVHSkeleton *skeleton, Camera *camera, HandleData *selectionData, bool skipUpdate)
{
    // Update the whetehr the mouse is pressed
    if(IsMouseButtonDown(MOUSE_LEFT_BUTTON) && !skipUpdate)
    {
        selectionData->onPressed = !selectionData->pressed;
        selectionData->pressed = true;
    }
    else
    {
        // Reset selection
        selectionData->pressed = false;
        selectionData->onPressed = false;
        selectionData->selectedJoint = nullptr;
        selectionData->selectedHandle = nullptr;
    }

    if(!skipUpdate)
    {
        // Get the mouse ray in world coordinates
        Ray mouseRay = GetMouseRay(GetMousePosition(), *camera);

        // Whether a handle has been highlighted
        bool highlightedJoint = false;

        UpdateIKHandles(mouseRay, selectionData, &highlightedJoint);

        for(unsigned int i = 0; i < skeleton->roots.size(); ++i)
        {
            UpdateJointHandleRecursive(skeleton->roots[i], mouseRay, selectionData, &highlightedJoint);
        }
    }

}

/// @brief Computes the drag position of a handle based on mouse position
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current hadle data
/// @return The target position of the handle
Vector3 ComputeTarget(Ray mouseRay, HandleData *selectionData)
{
    // Handles can be moved in 6 directions
    Vector3 directions[6];
    directions[0] = (Vector3){1,0,0};
    directions[1] = (Vector3){-1,0,0};
    directions[2] = (Vector3){0,1,0};
    directions[3] = (Vector3){0,-1,0};
    directions[4] = (Vector3){0,0,1};
    directions[5] = (Vector3){0,0,-1};

    // Each direction has corresponding axis colours
    Color axisColors[6];
    axisColors[0] = (Color){255, 0, 0, 255};
    axisColors[1] = (Color){125, 0, 0, 255};
    axisColors[2] = (Color){0, 255, 0, 255};
    axisColors[3] = (Color){0, 125, 0, 255};
    axisColors[4] = (Color){0, 0, 255, 255};
    axisColors[5] = (Color){0, 0, 125, 255};

    // Find distance from joint
    float mag = Vector3Distance(mouseRay.position, selectionData->startingPosition);
    Vector3 roughTargetPosition = Vector3Add(mouseRay.position, Vector3Scale(Vector3Normalize(mouseRay.direction), mag));

    // Find Vector
    Vector3 rawDifference = Vector3Subtract(roughTargetPosition, selectionData->startingPosition);
    mag = Vector3Length(rawDifference);

    // Snap Vector
    // Default closest angle is direction 0
    float closestAngle = Vector3Angle(rawDifference, directions[0]);
    Vector3 closestDirection = directions[0];
    Color axisColor = axisColors[0];

    // Iterate through each direction and find the closest one
    for(int i = 1; i < 6; ++i)
    {
        float angle = Vector3Angle(rawDifference, directions[i]);
        if(angle < closestAngle)
        {
            closestAngle = angle;
            closestDirection = directions[i];
            axisColor = axisColors[i];
        }
    }

    // Draw the axis that we're currently snapping to to guide the user
    Ray axisRay;
    axisRay.position = selectionData->startingPosition;
    axisRay.direction = Vector3Scale(closestDirection, 10);
    DrawRay(axisRay, axisColor);

    // Snap to axis and return the resultant postion
    return Vector3Add(selectionData->startingPosition, Vector3Scale(closestDirection, mag));
}

/// @brief Update the handles for a joint and its children
/// @param joint The joint to update
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current handle data
/// @param jointHighlighted Whether a joint has been highlightes
void UpdateJointHandleRecursive(Joint* joint, Ray mouseRay, HandleData *selectionData, bool *jointHighlighted)
{
    // Handle selection
    // If a joint hasn't been highlighted
    Color handleColor = HANDLE_COLOR;

    // If this joint is selected, update the colour
    if(selectionData->selectedHandle != nullptr && selectionData->selectedHandle->joint == joint)
    {
        handleColor = HANDLE_COLOR_SELECTED;
    } // If this joint is subject to IK constraints, change the colour
    else if(selectionData->currentHandles.count(joint))
    {
        handleColor = HANDLE_COLOR_TARGETED;
    }

    // Pressing on a joint will create a new IK Handle
    // If there isn't a currently selected joint and there isnt a curretly selected handle
    if(selectionData->selectedJoint == nullptr && selectionData->selectedHandle == nullptr && !*jointHighlighted)
    {
        // Get the mouse ray intersection of the joint
        RayCollision rayIntersect = GetRayCollisionSphere(mouseRay, joint->worldPosition, HANDLE_COLLISION_SIZE);

        if(rayIntersect.hit)
        {
            *jointHighlighted = true;
            
            // If we just pressed the mouse
            if(selectionData->onPressed)
            {
                // Set the starting position
                selectionData->startingPosition = joint->worldPosition;
                selectionData->selectedJoint = joint;

                // Create a new ik handle
                Handle ikHandle = {joint->worldPosition, joint};
                selectionData->currentHandles[joint] = ikHandle;
                selectionData->selectedHandle = &selectionData->currentHandles[joint];
            }
            // Change colour to indicate the mouse is over the handle
            handleColor = HANDLE_COLOR_HIGHLIGHTED;
        }
    }

    // Draw this handle
    DrawHandle(joint->worldPosition, handleColor);

    // Update child handles
    for(unsigned int i = 0; i < joint->childJoints.size(); ++i)
    {
        UpdateJointHandleRecursive(joint->childJoints[i], mouseRay, selectionData, jointHighlighted);
    }
}

/// @brief Update all IK handles
/// @param mouseRay The ray cast by the mouse in world coords
/// @param selectionData The current handle data
/// @param jointHighlighted Whether a joint has been highlightes
void UpdateIKHandles(Ray mouseRay, HandleData *selectionData, bool *jointHighlighted)
{
    // Iterate through all joint - IK handle pairs
    std::map<Joint*, Handle>::iterator handleIterator = selectionData->currentHandles.begin();

    while (handleIterator != selectionData->currentHandles.end())
    {
        Color handleColor = IK_HANDLE_COLOR;

        Joint *joint = handleIterator->first;
        Handle *handleRef = &selectionData->currentHandles[joint];

        // If this is the currently selected handle
        if(selectionData->selectedHandle == handleRef)
        {
            // Update the position
            Vector3 pos = ComputeTarget(mouseRay, selectionData);
            handleRef->position = pos;
        }

        // If there isn't anything selected
        if(selectionData->selectedJoint == nullptr && selectionData->selectedHandle == nullptr && !*jointHighlighted)
        {
            // Get the ray intersection of this ik handle
            RayCollision rayIntersect = GetRayCollisionSphere(mouseRay, handleRef->position, HANDLE_COLLISION_SIZE);

            if(rayIntersect.hit)
            {
                *jointHighlighted = true;
                
                // If we just pressed the mouse
                if(selectionData->onPressed)
                {
                    // Select this handle
                    selectionData->selectedHandle = handleRef;
                    selectionData->startingPosition = handleRef->position;
                }

                handleColor = HANDLE_COLOR_HIGHLIGHTED;
            }
        }

        // Draw the handle and a line to the joint it's targeting
        DrawHandle(handleRef->position, handleColor);
        DrawLine3D(handleRef->position, handleRef->joint->worldPosition, HANDLE_COLOR_TARGETED);
        handleIterator++;
    }
    
}

/// @brief Draws a handle
/// @param position
/// @param handleColor
void DrawHandle(Vector3 position, Color handleColor)
{
    DrawCube(position, HANDLE_SIZE, HANDLE_SIZE, HANDLE_SIZE, handleColor);
}