#include "Kinematics.h"
#include <eigen3/Eigen/Dense>

const float jacobianDelta = 0.00001f;
const float kinematicLambda = 0.9f;
const float distanceThreshold = 0.5f;

/// @brief Update the world position of a joint and its children
/// @param joint The joint to update
/// @param workingMatrix The local coordinate system
void UpdateJointCoordinatesRecursive(Joint *joint, Matrix workingMatrix)
{
    Vector4 homogenousCoords = {0,0,0,1};

    // Construct the translation matrix
    Matrix translation = joint->translationMatrix;
    translation.m3 += joint->initialsOffset.x;
    translation.m7 += joint->initialsOffset.y;
    translation.m11 += joint->initialsOffset.z;
    
    // Construct the trasform matrix (using translation and rotation)
    workingMatrix = MatrixMultiply(workingMatrix, translation);
    workingMatrix = MatrixMultiply(workingMatrix, joint->rotationMatrix);

    // Calculate the homogenous
    homogenousCoords = Matrix4VectorMultiplication(workingMatrix, homogenousCoords);

    // Calculate the cartesian world position
    Vector3 cartesian = {homogenousCoords.x, homogenousCoords.y, homogenousCoords.z};
    // Set the joints world position
    joint->worldPosition = cartesian;

    // Recurse
    for(int i = 0; i < joint->childJoints.size(); ++i)
    {   
        UpdateJointCoordinatesRecursive(joint->childJoints[i], workingMatrix);
    }
}

/// @brief Updates the world position coordinates of each joint in a skeleton
void UpdateSkeletonCoordinates(BVHSkeleton *skeleton)
{
    // Recursively update joint coords
    for(int i = 0; i < skeleton->roots.size(); ++i)
    {
        UpdateJointCoordinatesRecursive(skeleton->roots[i], MatrixIdentity());
    }
}

/// @brief Computes an inverse kinematics iteration based on a collection of targets and their corresponding joints
/// @param jointHandlePairs A map of joints subject to positional constraints and their IK handles
/// @param skeleton The skeleton to compute IK on
void ComputeInverseKinematicsIteration(std::map<Joint *, Handle>& jointHandlePairs, BVHSkeleton *skeleton)
{
    // Create arrays for the joint handle pairs
    unsigned int numJoints = jointHandlePairs.size();
    Joint **jointBuffer = (Joint**)malloc(sizeof(Joint) * numJoints);
    Vector3 *targetBuffer = (Vector3*)malloc(sizeof(Vector3) * numJoints);

    // iterate through each joint handle pair
    std::map<Joint*, Handle>::iterator handleIterator = jointHandlePairs.begin();
    int i = 0;

    while (handleIterator != jointHandlePairs.end() && i < numJoints)
    {
        // Set it in the array
        jointBuffer[i] = handleIterator->first;
        targetBuffer[i] = handleIterator->second.position;

        ++i;
        handleIterator++;
    }

    ComputeInverseKinematicsIteration(jointBuffer, targetBuffer, numJoints, skeleton);

    free(jointBuffer);
    free(targetBuffer);
}

/// @brief Computes an inverse kinematics iteration based on a collection of targets and their corresponding joints
/// @param joints The joints subject to positional constraints
/// @param targets The joint targets
/// @param numPairs The number of joint target pairs
/// @param skeleton The skeleton to compute IK on
void ComputeInverseKinematicsIteration(Joint **joints, Vector3 *targets, int numPairs, BVHSkeleton *skeleton)
{
    unsigned int numChannels = skeleton->ikChannels.size();

    // Make a copy of the channel buffer
    float *bufferCopy = (float*)malloc(sizeof(float) * skeleton->channelCounter);
    memcpy(bufferCopy, skeleton->channelBuffer, sizeof(float) * skeleton->channelCounter);

    // Calculate original
    Eigen::MatrixXf originalPositions(numPairs * 3, 1);
    Eigen::MatrixXf V(numPairs * 3, 1);

    // Update the skeleton
    skeleton->UpdateJointMatrices();
    UpdateSkeletonCoordinates(skeleton);

    // Iterate through the joint handle pairs
    for(int i = 0; i < numPairs; ++i)
    {
        // Get the original position of the joint
        originalPositions(i*3, 0) = joints[i]->worldPosition.x;
        originalPositions(i*3+1, 0) = joints[i]->worldPosition.y;
        originalPositions(i*3+2, 0) = joints[i]->worldPosition.z;

        // Get the vector from the joint to the goal
        Vector3 currentV = Vector3Subtract(targets[i], joints[i]->worldPosition);

        // Add a distance threshold (to prevent wobbling when the joint is close)
        if(Vector3Length(currentV) > distanceThreshold)
        {
            V(i*3, 0) = currentV.x;
            V(i*3+1, 0) = currentV.y;
            V(i*3+2, 0) = currentV.z;
        }
        else
        {
            V(i*3, 0) = 0;
            V(i*3+1, 0) = 0;
            V(i*3+2, 0) = 0;
        }

    }

    // Construct a jacobian matrix of channels and target values
    Eigen::MatrixXf jacobian(numPairs * 3, numChannels);

    // Iterate through each channel
    for(unsigned int i = 0; i < numChannels; ++i)
    {
        // Alter the channel
        *skeleton->ikChannels[i] += jacobianDelta;

        // Update coords
        skeleton->UpdateJointMatrices();
        UpdateSkeletonCoordinates(skeleton);

        // Get delta positions
        Eigen::MatrixXf delta(numPairs * 3, 1);
        for(int j = 0; j < numPairs; ++j)
        {
            delta(j*3, 0) = joints[j]->worldPosition.x;
            delta(j*3+1, 0) = joints[j]->worldPosition.y;
            delta(j*3+2, 0) = joints[j]->worldPosition.z;
        }

        delta = (delta - originalPositions) * 1/jacobianDelta;

        // Fill in this channel's column in the jacobian
        for(int j = 0; j < numPairs * 3; ++j)
        {
            jacobian(j, i) = delta(j, 0); 
        }

        // Reset the skeletons channel buffer
        memcpy(skeleton->channelBuffer, bufferCopy, sizeof(float) * skeleton->channelCounter);
    }

    // Transpose the jacobian
    Eigen::MatrixXf JJt = jacobian * jacobian.transpose();
    int JJtSize = int(JJt.rows());

    // Construct lambda identity
    Eigen::MatrixXf identity;
    identity.setIdentity(JJtSize, JJtSize);
    identity = identity * powf(kinematicLambda, 2);

    JJt = JJt + identity;
    JJt = JJt.inverse();

    // Final result
    Eigen::MatrixXf newChannels(numChannels, 1);
    newChannels = jacobian.transpose() * JJt * V;

    // Write the result to the ik channels. This points to channels in the skeletons channel buffer
    for(unsigned int i = 0; i < numChannels; ++i)
    {
        *skeleton->ikChannels[i] += newChannels(i,0);
    }

    // Update the skeleton
    skeleton->UpdateJointMatrices();
    UpdateSkeletonCoordinates(skeleton);
}

