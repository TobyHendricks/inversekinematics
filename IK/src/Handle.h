#pragma once
#include "raylib.h"
#include "raymath.h"
#include "Joint.h"

// An IK Handle
struct Handle
{
    Vector3 position;
    Joint * joint;
};